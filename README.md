# Waqt
A sane date and time API for Emacs

## Installation
You can get `waqt` from https://framagit.org/contrapunctus/waqt

## Prior art
https://github.com/emacs-php/emacs-datetime
* 👍 Can convert between different formats
* 👍 Built-in format definitions store spec URLs too
* 👎 No operations
* 👎 No types defined

## Contributions and contact
Feedback and MRs very welcome. 🙂 [doc/hacking.md](doc/hacking.md) contains an introduction to the codebase.

Contact the creator and other Emacs users in the Emacs room on the Jabber network - [xmpp:emacs@salas.suchat.org?join](xmpp:emacs@salas.suchat.org?join) ([web chat](https://inverse.chat/#converse/room?jid=emacs@salas.suchat.org))

(For help in getting started with Jabber, [click here](https://xmpp.org/getting-started/))

## License
Waqt is released under your choice of [Unlicense](https://unlicense.org/) and the [WTFPL](http://www.wtfpl.net/).

(See files [LICENSE](LICENSE) and [LICENSE.1](LICENSE.1)).
