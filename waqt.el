;;; waqt.el --- a sane date and time API for Emacs

(require 'waqt-formats)
(require 'eieio)

;;; Commentary:
;;

(defclass waqt-format ()
  ((value :initform :value)))

(cl-defgeneric waqt-to-unix (data)
  "Convert DATA to UNIX epoch time.")

(cl-defgeneric waqt-from-unix (input target-format-object)
  "Convert a UNIX epoch time value to a different format.")

(defun waqt (data format &key time timezone from to)
  "Return DATA in FORMAT.
DATA can be 'date, 'time, 'date-time, 'period, or (TBD)
individual fields like 'year, 'month, 'date, 'hours, 'seconds,
'tzoffset, 'dayofweek.

FORMAT can be the name of any subclass of class `format'.

TIME defaults to the current time.

TIMEZONE defaults to the local timezone.

FROM and TO default to the current time."
  )

(provide 'waqt)

;;; waqt.el ends here
