;;; waqt-formats.el --- Format definitions for Waqt

;;; Commentary:
;;

(defclass waqt-unix ()
  ((value :initarg :value
          :initform (current-time)
          :type (or list integer)))
  "Class for chronological information as UNIX epoch time.")

(defclass waqt-iso-8601 ()
  ((value :initarg :value
          :initform (format-time-string "%FT%T%z")
          :type string))
  "Class for chronological information in ISO-8601 string format.")

(cl-defmethod waqt-to-unix ((input waqt-iso-8601))
  "Convert ISO-8601 to a time value (see (info \"(elisp)Time of Day\"))."
  (waqt-unix :value
         (parse-iso8601-time-string
          (oref input :value))))

(cl-defmethod waqt-from-unix ((input waqt-unix) (output waqt-iso-8601))
  "Convert a UNIX epoch time value to an ISO-8601 string."
  (oset output
        :value
        (format-time-string "%FT%T%z"
                            (oref input :value))))

(provide 'waqt-formats)

;;; waqt-formats.el ends here
