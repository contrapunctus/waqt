* Code issues
  1. waqt-to-unix returns a new object, waqt-from-unix modifies the second object it is provided. Inconsistent...and inelegant?
  2. Ideally, we want an abstract `waqt-format` class which all formats inherit from, but I don't know of a way to add additional properties to an inherited slot. (e.g. :initform and :type, which will differ between subclasses)
  3. [ ] Create a macro to make conversion calls more concise
     * (current) unacceptably long? ~(defvar foo (waqt-iso-8601 :value <VALUE>))~
     * preferable? ~(waqt-defiso foo <VALUE>)~
